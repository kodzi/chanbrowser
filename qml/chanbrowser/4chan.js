﻿var FOURCHAN_API = "https://api.4chan.org/"
var FOURCHAN_BOARDS = "http://api.4chan.org/boards.json"
var USER_AGENT = "ChanBrowser/0.1 (Nokia; Qt; MeeGo Harmattan)"

function getBoards() {
    //Empty the listmodel just in case
    boardsView.model.clear()
    console.debug("Get Boards")
    var URL = FOURCHAN_API+"boards.json"
    var xhr = new XMLHttpRequest;

    xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status == 200)
                {
                    var a = JSON.parse(xhr.responseText);

                    //adding board information one by one
                    for (var i = 0; i < a.boards.length; i++)
                    {
                        boardsView.model.append(a.boards[i]);
                    }
                    appWindow.busy = false
                }
                else{ appWindow.busy = true }
            }
    xhr.open("GET", URL);
    xhr.setRequestHeader("User-Agent", USER_AGENT)
    xhr.send();

}

function getCatalog(board)
{
    console.debug("Get Board "+board)
    //Catalog
    var URL = FOURCHAN_API+board+"/catalog.json"
    var xhr = new XMLHttpRequest;
    console.debug(URL)
    xhr.open("GET", URL);
    //console.debug("GET start")
    xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status == 200)
                {
                    //console.debug("REQUEST DONE")
                    var a = JSON.parse(xhr.responseText);

                    //console.debug("PARSING BOARD WITH RESPONSETEXT "+xhr.responseText)
                    //var count = 0;
                    for(var j = 0; j < a.length; j++)
                    {
                        for(var i = 0; i < a[j].threads.length; i++)
                        {
                            var threads = a[j].threads[i]
                            //if has width, means that the file is an image
                            if(threads.w)
                            {
                                imageCatalog.append(threads);
                            }
                        }
                    }
                    appWindow.busy = false

                    //console.debug("PARSING DONE, "+count+" threads added")
                }


            }
    xhr.setRequestHeader("User-Agent", USER_AGENT)
    xhr.send();
}

function getBoard(board, pagenumber)
{

    console.debug("Get Board "+board)
    //Catalog
    //var URL = FOURCHAN_API+board+"/catalog.json"
    console.debug("Board: "+board+" Page: "+pagenumber)

    //individual page http(s)://api.4chan.org/board/pagenumber.json
    var pageURL = FOURCHAN_API+board+"/"+pagenumber+".json"
    var xhr = new XMLHttpRequest;
    console.debug(pageURL)
    xhr.open("GET", pageURL);
    //console.debug("GET start")
    xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status == 200)
                {
                    //console.debug("REQUEST DONE")
                    var a = JSON.parse(xhr.responseText);

                    //console.debug("PARSING BOARD WITH RESPONSETEXT "+xhr.responseText)
                    //var count = 0;
                    for(var j = 0; j < a.threads.length; j++)
                    {
                        var threads = a.threads[j]
                        boardView.model.append({"page": pagenumber, "posts": threads.posts[0]})
                        //count++;
                    }
                    //appWindow.busy = false

                    //console.debug("PARSING DONE, "+count+" threads added")
                }
                //else{appWindow.busy = true }

            }
    xhr.setRequestHeader("User-Agent", USER_AGENT)
    xhr.send();
}

function getThread(board, threadnumber)
{
    console.debug("Get Thread "+threadnumber)
    threadView.model.clear()
    var URL = FOURCHAN_API+board+"/res/"+threadnumber+".json"
    var xhr = new XMLHttpRequest;
    xhr.open("GET", URL);
    xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status == 200)
                {
                    var a = JSON.parse(xhr.responseText);
                    for (var i = 0; i < a.posts.length; i++) {
                        threadView.model.append(a.posts[i]);
                    }
                    appWindow.busy = false
                }else{ appWindow.busy = true }
            }
    xhr.setRequestHeader("User-Agent", USER_AGENT)
    xhr.send();
}

function linkify(text) {
    var exp = /(\b(https?|ftp|file|magnet):[\/?]*[\/x]*[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    var exp2 = /(<wbr>)/ig;
    // Find a way to include this string into the expression
    var cont = text.replace(exp2,"");
    return cont.replace(exp,"<a href='$1'>$1</a>");
}

