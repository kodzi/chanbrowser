﻿// Delegation for all threads in one board
// necessary to set image smoothing to false
// when interacting with list ?


import QtQuick 1.1
import com.nokia.meego 1.0
import "constants.js" as UI
import "4chan.js" as API

Component{
    id: main
    Item{
        width: (inPortrait)? 480:854
        height: threadContainer.height +  UI.MARGIN/2


        MouseArea{
            id: containerArea
            anchors.fill: parent
            onClicked:{
                console.debug("thread clicked")
                threadPage.board = board
                threadPage.threadnumber = model.posts.no
                threadPage.threadName = ((model.posts.sub)? model.posts.sub: model.posts.no)
                threadPage.getDb()
                pageStack.push(threadPage)
            }
        }
        //threadContainer
        Item
        {
            id: threadContainer
            height: ((threadImage.status === Image.Error || threadImage.status === Image.Loading)? 70:postContainer.height)
                    + threadDetails.height
                    + separatorHo.height + UI.MARGIN;
            width: parent.width - UI.MARGIN

            anchors
            {
                //verticalCenter: parent.verticalCenter;
                top: parent.top; topMargin: UI.MARGIN /2
                left: parent.left; leftMargin: UI.MARGIN /2
            }

            //threadDetails
            Item
            {
                id: threadDetails
                width: parent.width
                height: UI.MARGIN
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                property alias subjectTruncated: threadNameOrNo.truncated
                //Number of Replies and Images
                Row
                {
                    id: rAndP
                    spacing: UI.MARGIN /2
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    Text {
                        text: ((model.posts.sub || threadDetails.subjectTrunkated)? "<b>P:</b> ":"<b>Posts:</b> ")+model.posts.replies
                        font.pixelSize: UI.THREAD_CONTENT
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT

                    }
                    Text {
                        text: ((model.posts.sub || threadDetails.subjectTrunkated)? "<b>I:</b> ":"<b>Images:</b> ")+ (model.posts.images + 1)
                        font.pixelSize: UI.THREAD_CONTENT
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT

                    }
                }
                //topic name, if not available
                Row
                {
                    id:threadSubject
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: UI.MARGIN/2
                    width: parent.width - rAndP.width - UI.MARGIN
                    Text{
                        id: threadNameOrNo
                        text: (model.posts.sub)? ("<b>T: </b>"+model.posts.sub):("<b>No.</b>"+model.posts.no)
                        wrapMode: Text.WordWrap     //wrap text into container

                        font.pixelSize: UI.THREAD_CONTENT
                        elide: Text.ElideRight
                        clip: true
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        //truncatedChanged: { threadDetails.subjectTruncated = threadNameOrNo.truncated }
                    }
                    Text{
                        id: threadTime
                        text: (model.posts.now)
                        font.pixelSize: UI.THREAD_CONTENT
                        elide: Text.ElideRight
                        clip: true
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        visible: (inPortrait)? false:true
                    }
                }



            }

            Row
            {
                id: postContainer
                width: parent.width
                anchors.top: threadDetails.bottom
                anchors.topMargin: UI.MARGIN/2
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: UI.MARGIN/4
                height: (model.posts.tn_h)? threadImage.paintedHeight:postContent.paintedHeight

                //threadImage
                Image{
                    //in case flash board: no image but has filename
                    visible: (model.posts.tn_w)? true:false
                    id: threadImage
                    source: (model.posts.filename)? ( UI.URL_THUMB_BASE+board+"/thumb/"+model.posts.tim+"s.jpg"):""
                    fillMode:Image.PreserveAspectFit
                    //Image height decide the height of the threadpost
                    height: (model.posts.tn_h > model.posts.tn_w)? (Math.min(model.posts.tn_h,140)):((threadImage.width/model.posts.tn_w)*model.posts.tn_h)
                    width: (model.posts.tn_h > model.posts.tn_w)? ((threadImage.height/model.posts.tn_h)*model.posts.tn_w):(Math.min(model.posts.tn_w,140))
                    smooth: true
                    clip: true
                    MouseArea{
                        id: thumbArea
                        anchors.fill: parent
                        onClicked:{
                            singleImage.source = UI.URL_IMG_BASE+board+"/src/"+model.posts.tim+model.posts.ext
                            console.debug("thumbnail clicked")
                            pageStack.push(singleImage)
                            //pageStack.push(galleryPage)
                        }
                    }
                    Loader {
                        id: busyLoader
                        sourceComponent: (threadImage.status === Image.Loading) ? updatingIndicator : undefined
                        visible: (threadImage.status === Image.Loading)
                        anchors.centerIn: parent.Center
                        Component {
                            id: updatingIndicator;
                            BusyIndicator{
                                id: busyIndicator
                                BusyIndicatorStyle { size: "medium" }
                                running: busyLoader.visible
                            }
                        }
                    }
                    onStatusChanged: {
                        if( threadImage.status == Image.Error)
                        {
                            threadImage.source = "image://theme/icon-m-common-fault"
                            threadImage.width = 70
                            threadImage.height = 70
                            console.debug("Image error, deleted? Showing holderimage")

                            busy = false
                        }else if( threadImage.status === Image.Ready)
                        {
                            busy = false
                        }else if (threadImage.status === Image.Loading)
                        {
                            busy = true
                        }
                    }
                }
                Image{
                    id: separatorVe
                    visible: (model.posts.tn_w || threadImage.status === Image.Ready)? true:false
                    height: threadImage.paintedHeight - UI.MARGIN*2
                    source: "image://theme/meegotouch-separator"+((darkMode)? "-inverted":"") +"-background-vertical"
                    fillMode: Image.TileVertically

                }
                //postContent
                Text {
                    id: postContent
                    height: (model.posts.tn_h)? threadImage.paintedHeight:70
                    width: parent.width - ((model.posts.tn_w)? threadImage.width:0) - UI.MARGIN
                    font.pixelSize: UI.THREAD_CONTENT
                    text: (model.posts.com)? ((linkifyUrls)? API.linkify(model.posts.com): model.posts.com):("<center><i>empty</i></center>")
                    textFormat: Text.RichText
                    onLinkActivated: {
                        //console.debug(model.posts.com)
                        console.debug(link)
                        Qt.openUrlExternally(link)
                        boardPage.bannerText = "Opening browser..."
                        boardPage.showBanner()
                    }
                    color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                    elide: Text.ElideRight
                    wrapMode: Text.Wrap
                    clip: true
                }
            }





            //Item separator, separatoreHo
            Image{
                id: separatorHo
                width: parent.width
                source: "image://theme/meegotouch-separator"+((darkMode)? "-inverted":"") +"-background-horizontal"
                fillMode: Image.TileHorizontally
                anchors {
                    top: parent.bottom
                    left: parent.left;
                    right: parent.right;
                }

            }
        }
    }
}
