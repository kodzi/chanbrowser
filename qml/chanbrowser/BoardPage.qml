﻿// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1
import "4chan.js" as Api
import "constants.js" as UI

Page {
    //id: boardPage

    property int pagenumber
    property int maxpage
    property int t_perpage
    property string board
    property string boardName: "Board"
    property alias bannerText: banner.text
    function getDb(){
        boardView.model.clear()
        Api.getBoard(board, pagenumber)
    }

    function showBanner(){
        banner.timerShowTime = 3000 // ms
        banner.text = bannerText
        banner.show();
    }

    function createPageDialog(){
        for(var i = 0; i <= maxpage; i++)
        {
            singleSelectionDialog.model.append({"name": "#"+i})
        }
    }
    function showPageDialog(){
        singleSelectionDialog.open();
    }
    function getCatalog(){
        imageCatalog.clear()
        Api.getCatalog(board);
    }

    SelectionDialog{
        id: singleSelectionDialog
        titleText: "Move To Page"
        selectedIndex: pagenumber
        model: ListModel{}
        onSelectedIndexChanged:
        {
            pagenumber = selectedIndex
            getDb();
        }
    }

    //background
    Rectangle{
        width:parent.width; height: parent.height
        anchors.fill: parent
        color: (darkMode)? UI.TOMORROW_BG:UI.YOTSUBA_BG
    }
    //toolBar
    ToolBarLayout{
        id: boardTools
        ToolIcon{
            platformIconId: "toolbar-back" + (enabled ? "" : "-dimmed")
            onClicked: pageStack.pop()
        }
        ToolIcon{
            platformIconId: "toolbar-refresh"
            visible: !busy
            onClicked: {
                Api.getBoard(board,pagenumber)
            }
        }

        ToolIcon{
            platformIconId: "toolbar-gallery"
            visible: !busy
            onClicked: {
                console.debug("TODO: Gallery mode, still not working")
                //                getCatalog();
//                galleryPage.imageC = imageCatalog
//                pageStack.push(galleryPage);
            }
        }

        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: { mainMenu.open() }
        }
    }


    ListView{
        id: boardView
        anchors {
            top: pageHeader.bottom
            bottom: parent.bottom;
            left: parent.left; right:parent.right
        }

        model: ListModel{}
        delegate: BoardDelegate{id: boardDelegate}
        orientation: ListView.Vertical


        //onAtYEndChanged: { console.debug("TODO: Load next page")}
    }


    ScrollDecorator { flickableItem: boardView}


    PageHeader{id: pageHeader}
    InfoBanner{id: banner}
    ThreadPage{id: threadPage }
    //SingleImage{id: singleImage }
    GalleryPage{id: galleryPage}




    Component.onCompleted: {
        console.debug("BoardPage loaded")
    }
    onStatusChanged: if(boardPage.status === PageStatus.Active){ pageHeader.activePage = "BoardPage" }
    tools: boardTools
}
