﻿// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import "constants.js" as UI

Page{
    property alias imageC: galleryView.model

    width: parent.width
    height: parent.height

    ListView{
        id: galleryView
        anchors.fill: parent
        model: ListModel{}
        delegate:   PinchableImage{
            id: pinchImage
            visible: true
            anchors.fill: parent
            source: (model.filename)? (UI.URL_IMG_BASE+board+"/src/"+model.tim+model.ext):""
        }
        boundsBehavior: Flickable.DragOverBounds
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        interactive: moving //|| (model.count < 1)? !currentItem.allowDelegateFlicking:true
    }

    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back" + (enabled ? "" : "-dimmed")
            onClicked: pageStack.pop()
        }
    }
}
