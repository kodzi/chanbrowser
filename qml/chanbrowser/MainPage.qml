﻿import QtQuick 1.1
import com.nokia.meego 1.0
import "4chan.js" as Api
import "constants.js" as UI

Page {
    id: mainPage
    orientationLock: PageOrientation.LockPortrait
    //Background color; Dark: Tomorrow, Light: Yotsuba
    Rectangle{
        width:parent.width; height: parent.height
        anchors.fill: parent
        color: (darkMode)? UI.TOMORROW_BG:UI.YOTSUBA_BG
    }
    property string backgroundPage: "image://theme/meegotouch-view-header-button-inverted"
    property string itemCodeBG: "image://theme/meegotouch-list-background"
    property string chanName: "<b>4Chan</b>"
    tools: mainTools

    function getDb()
    {
        Api.getBoards()
    }

    //boardDelegate
    Component{
        id: boardsDelegate
        Item {
            id: boardItem
            //if nsfwMode = False => hide nsfw boards
            visible: {
                if(nsfwMode === false)
                {
                    if(model.ws_board === 0)
                    {
                        //console.debug("Board NSFW, not showing: " + model.board)
                        return nsfwMode;
                    }
                    //console.debug("1. Showing board: " + model.board)
                    return !nsfwMode;
                }
                else
                {
                    //console.debug("2. Showing board: " + model.board)
                    return nsfwMode;
                }
            }
            width: (boardItem.visible)?  parent.width:0
            height: (boardItem.visible)? 67:0
            anchors { left: parent.left; right: parent.right  }
            //handle mouse operations
            MouseArea{
                id: mouseArea
                anchors.fill:parent
                onClicked:{
                    boardsView.currentIndex = index
                    boardPage.pagenumber = 0
                    boardPage.board = model.board
                    boardPage.boardName = model.title
                    boardPage.maxpage = model.pages
                    console.debug("Passing: "+model.board)
                    boardPage.status = PageStatus.Activating
                    boardPage.getDb()
//                    Api.getCatalog(model.board)
                    pageStack.push(boardPage)
                }
            }

            //Content
            Item {
                id: boardItemContainer
                anchors {
                    fill:parent
                    topMargin: UI.MARGIN/2
                    rightMargin: UI.MARGIN; bottomMargin: UI.MARGIN/2
                    left: parent.left; leftMargin: UI.MARGIN
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
                //boardCode
                Item
                {
                    id: boardCode
                    width: parent.height + UI.MARGIN
                    height: parent.height
                    //color: "green"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    Text{
                        font.pixelSize: UI.CODE_SIZE
                        text: "/"+model.board+"/"
                        font.bold: true
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                //boardName
                Text{
                    id: boardName
                    width: parent.width - boardCode.width
                    font.pixelSize: UI.TITLE_SIZE
                    color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                    text: model.title
                    anchors.left: boardCode.right
                    anchors.leftMargin: UI.MARGIN
                    anchors.verticalCenter: parent.verticalCenter
                }
                //nsfwTag
                Text {
                    id: nsfwTag;
                    text: (model.ws_board === 0)? "nsfw":"";
                    color: (darkMode)? "white":"black"
                    font.italic: true
                    font.pixelSize: UI.TITLE_SIZE - 4
                    anchors.right: boardFavorite.left
                    anchors.rightMargin: UI.MARGIN/2
                    anchors.verticalCenter: parent.verticalCenter
                }
                //favorite Star
                Image
                {
                    //temp variable, until favorite saving function implemented
                    property bool favorite: false
                    id: boardFavorite
                    height: parent.height; width: 48 //from image
                    fillMode: Image.PreserveAspectFit
                    source: "image://theme/icon-m-common-favorite"+((favorite)? "-mark":"-unmark")+((darkMode)? "-inverse":"")
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    smooth: true
                    MouseArea{
                        anchors.fill: parent
                        onClicked: { parent.favorite = !parent.favorite }
                    }
                }

            }
            //separator
            Image{
                id: separator
                width: parent.width
                source: "image://theme/meegotouch-separator"+((darkMode)? "-inverted":"") +"-background-horizontal"
                fillMode: Image.TileHorizontally
                anchors {
                    top: parent.bottom
                    left: parent.left
                    right: parent.right
                }

            }

        }

    }

    ListView{
        id: boardsView
        model: ListModel{}
        delegate: boardsDelegate
        anchors{
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        orientation: ListView.Vertical
        //snapMode: ListView.SnapToItem
        onCurrentIndexChanged:
        {
            console.debug("Current index: "+boardsView.currentIndex)
            //positionViewAtIndex(boardsView.currentIndex)
        }


    }

    ScrollDecorator { flickableItem: boardsView}
    PageHeader{ id: pageHeader }
    BoardPage{id: boardPage }


    Component.onCompleted: {
        console.debug("MainPage Loaded")
    }
    onStatusChanged: if(mainPage.status === PageStatus.Active){ pageHeader.activePage = "MainPage" }

}
