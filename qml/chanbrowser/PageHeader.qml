﻿//Application header
import QtQuick 1.1
import com.nokia.meego 1.0
import "constants.js" as UI

Item{
    id: main

    property string title: ""
    property bool comboBoxVisible: false
    property string activePage: "Home"
    property string headerBG: "image://theme/meegotouch-viewheader"
    //property alias showSwitch: nsfwSwitchContainer.visible

    height:UI.HEADER_HEIGHT
    width: parent.width

    Rectangle{
        id: headerBorder
        width: parent.width; height: parent.height
        color: (darkMode)? "#880000":"#ffccaa"
        MouseArea{
            id: headerFunction
            enabled: (activePage === "BoardPage")? true:false
            anchors.fill: parent
            onClicked:{
                boardPage.createPageDialog();
                boardPage.showPageDialog();
            }
        }


        Text {
            id: headerTitle
            color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
            font.pixelSize: UI.HEADER
            width: parent.width - UI.MARGIN*2 - headerPgNumber - busyLoader
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: UI.MARGIN
            text: {
                console.debug("Currentpage: "+activePage)
                switch (activePage)
                {
                case "BoardPage":
                    return boardPage.boardName;
                case "ThreadPage":
                    return threadPage.threadName;
                case "MainPage":
                    return mainPage.chanName;
                }
            }
            elide: Text.ElideRight
            clip: true
        }

        Row
        {
            id: headerPgNumber
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: UI.MARGIN
            spacing: UI.MARGIN/4
            layoutDirection: Qt.RightToLeft
            Text {
                //id: headerPgNumber
                visible: (boardPage.status === PageStatus.Active)? true:false
                font.pixelSize: UI.HEADER
                color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                text: "page: "+ boardPage.pagenumber
            }
            Loader {
                id: busyLoader
                sourceComponent: (busy)? updatingIndicator:undefined
                visible: busy
                Component {
                    id: updatingIndicator;
                    BusyIndicator{
                        id: busyIndicator
                        BusyIndicatorStyle { size: "small" }
                        running: busyLoader.visible
                        //visible: busy
                    }

                }
            }



        }


    }

    Image{
        id: separator
        width: parent.width
        source: "image://theme/meegotouch-groupheader"+((darkMode)? "-inverted":"") +"-background"
        fillMode: Image.TileHorizontally
        anchors.top: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
