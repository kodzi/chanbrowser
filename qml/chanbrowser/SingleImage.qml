// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    property string source

    width: parent.width
    height: parent.height
    PinchableImage{
        id: pinchImage
        visible: true
        anchors.fill: parent
    }
    tools: mainTools
    onStatusChanged: {
        pinchImage.source = source

    }
}
