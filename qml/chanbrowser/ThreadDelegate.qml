// Delegation for all threads in one board
// necessary to set image smoothing to false
// when interacting with list ?

import QtQuick 1.1
import com.nokia.meego 1.0
import "constants.js" as UI
import "4chan.js" as API
Component{

    id: main
    Item{
        width: (inPortrait)? 480:854
        height: postContainer.height +  UI.MARGIN/2
        MouseArea{
            id: containerArea
            anchors.fill: parent
            onClicked:{
                console.debug("post clicked")
            }
        }
        //postContainer
        Item
        {
            id: postContainer
            height: postContent.height + postDetails.height + separatorHo.height + UI.MARGIN;
            width: parent.width - UI.MARGIN - ((model.resto)? UI.MARGIN:0)
            anchors
            {
                //verticalCenter: parent.verticalCenter;
                top: parent.top; topMargin: UI.MARGIN /2
                right: parent.right; rightMargin: UI.MARGIN/2
            }
            //postDetail
            Column
            {
                id: postDetails
                //width: parent.width
                //fix when remove upper parent
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.verticalCenter: parent.verticalCenter
                // spacing: UI.MARGIN/6
                //postDetails
                Item
                {
                    //height: posterName.paintedHeight
                    width: parent.width
                    height: posterName.paintedHeight
                    Text
                    {
                        id: posterName
                        text: model.name
                        width: parent.width - postNo.paintedWidth - UI.MARGIN/2
                        wrapMode: Text.WordWrap     //wrap text into container
                        font.pixelSize: UI.THREAD_CONTENT
                        elide: Text.ElideRight
                        font.bold: true
                        clip: true
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        anchors.left: parent.left
                    }
                    //postNo
                    Text
                    {
                        id: postNo
                        text: "<b>No.</b>"+model.no
                        font.pixelSize: UI.THREAD_CONTENT
                        elide: Text.ElideRight
                        clip: true
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        anchors.right: parent.right
                    }
                }

                //fileDetails
                Item
                {
                    height: fileName.paintedHeight
                    width: parent.width
                    visible: (model.filename)? true:false
                    //fileName
                    Text
                    {
                        id: fileName
                        maximumLineCount: 1
                        //visible: (model.filename)? true:false
                        width: parent.width - fileResAndSize.width - UI.MARGIN/2
                        text: (model.filename)? model.filename+model.ext:""
                        font.pixelSize: UI.THREAD_CONTENT
                        elide: Text.ElideRight
                        wrapMode: Text.Wrap
                        clip: true
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        anchors.left: parent.left
                    }

                    //fileResAndSize
                    Text
                    {
                        id: fileResAndSize
                        visible: (model.tn_h)? true:false
                        text: ((model.w)? "("+model.w+"x"+model.h+") ":"")+((model.fsize/1000 < 1000)? (model.fsize/1000)+" KB":(model.fsize/1000000)+" MB")
                        font.pixelSize: UI.THREAD_CONTENT
                        color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                        anchors.right: parent.right
                    }
                }

            }

            //postContent
            Row
            {
                id: postContent
                width: parent.width
                anchors.top: postDetails.bottom
                anchors.topMargin: UI.MARGIN/2
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: UI.MARGIN/4
                //postImage
                Image
                {
                    visible: (model.tn_w)? true:false
                    id: postImage
                    source: (model.filename)? ( UI.URL_THUMB_BASE+board+"/thumb/"+model.tim+"s.jpg"):""
                    fillMode:Image.PreserveAspectFit
                    //Image height decide the height of the threadpost
                    height: (model.tn_h > model.tn_w)? (Math.min(model.tn_h,140)):((postImage.width/model.tn_w)*model.tn_h)
                    width: (model.tn_h > model.tn_w)? ((postImage.height/model.tn_h)*model.tn_w):(Math.min(model.tn_w,140))

                    smooth: true
                    clip: true
                    MouseArea{
                        id: thumbArea
                        anchors.fill: parent
                        onClicked:{
                            singleImage.source = UI.URL_IMG_BASE+board+"/src/"+model.tim+model.ext
                            console.debug("thumbnail clicked")
                            pageStack.push(singleImage)
                        }
                    }
                    Loader {
                        id: busyLoader
                        sourceComponent: (postImage.status === Image.Loading) ? updatingIndicator : undefined
                        visible: (postImage.status === Image.Loading)
                        anchors.verticalCenter: parent.verticalCenter
                        Component {
                            id: updatingIndicator;
                            BusyIndicator{
                                id: busyIndicator
                                BusyIndicatorStyle { size: "medium" }
                                running: busyLoader.visible
                            }
                        }
                    }
                    onStatusChanged: {
                        if( postImage.status == Image.Error)
                        {
                            console.debug("Image error, deleted? Showing holderimage")
                            postImage.source = "image://theme/icon-m-common-fault"
                            postImage.height = 70
                            postImage.width = 70
                            //busy = false
                        }else if( postImage.status === Image.Ready)
                        {
                            busy = false
                        }else if (postImage.status === Image.Loading)
                        {
                            busy = true
                        }
                    }
                }
                //postImage max height:
                Image{
                    id: separatorVe
                    height: postImage.paintedHeight - UI.MARGIN*2
                    source: "image://theme/meegotouch-separator"+((darkMode)? "-inverted":"") +"-background-vertical"
                    fillMode: Image.TileVertically
                    visible: (model.th_w || postImage.status !== Image.Ready)? false:true
                    anchors.verticalCenter: postImage.verticalCenter
                }
                Text {
                    id: postCom
                    //height: (model.tn_h)? postImage.paintedHeight:(70)
                    width: parent.width - postImage.paintedWidth - UI.MARGIN
                    font.pixelSize: UI.THREAD_CONTENT
                    text: (model.com)? ((linkifyUrls)? API.linkify(model.com): model.com):("<center><i>...empty...</i></center>")
                    textFormat: Text.RichText
                    onLinkActivated: {
                        //console.debug(model.com)
                        console.debug(link)
                        Qt.openUrlExternally(link)
                        threadPage.bannerText = "Opening browser..."
                        threadPage.showBanner()
                    }
                    //show all
                    color: (darkMode)? UI.TOMORROW_TEXT: UI.YOTSUBA_TEXT
                    wrapMode: Text.Wrap
                    //necessary ? if text too long?
                    elide: Text.ElideRight
                    clip: true
                }
            }
            //Item separator, separatoreHo
            Image{
                id: separatorHo
                width: parent.width
                source: "image://theme/meegotouch-separator"+((darkMode)? "-inverted":"") +"-background-horizontal"
                fillMode: Image.TileHorizontally
                anchors {
                    top: parent.bottom
                    left: parent.left;
                    right: parent.right;
                }

            }
        }
    }
}
