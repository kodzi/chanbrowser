﻿// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1
import "4chan.js" as Api
import "constants.js" as UI

Page{
    id: threadPage
    property string board
    property string threadnumber
    property string threadName: "Thread"
    property alias thread_model: threadView.model

    function getDb(){
        Api.getThread(board, threadnumber)
    }

    function showBanner(){
        banner.timerShowTime = 3000 // ms
        banner.text = bannerText
        banner.show();
    }

    //background
    Rectangle{
        width:parent.width; height: parent.height
        anchors.fill: parent
        color: (darkMode)? UI.TOMORROW_BG:UI.YOTSUBA_BG
    }
    ListView{
        id: threadView
        model: ListModel{}
        delegate: ThreadDelegate{ id: threadDelegate}
        anchors{
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        orientation: ListView.Vertical
    }
    PageHeader {
        id: pageHeader
    }
//    SingleImage {
//        id: singleImage
//    }
    InfoBanner {
        id: banner
    }

    tools: mainTools
    Component.onCompleted: {
        console.debug("ThreadPage loaded")
    }
    onStatusChanged: if(threadPage.status === PageStatus.Active){ pageHeader.activePage = "ThreadPage" }
}
