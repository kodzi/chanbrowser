﻿/*Boards*/
var TITLE_SIZE = 24
var CODE_SIZE = 22
var PAGES_SIZE = 16

/*Thread*/
var THREAD_ID = 16
var THREAD_TIME = 16
var THREAD_CONTENT = 18

/*UI*/
var HEADER = 32
var MARGIN = 16
var HEADER_HEIGHT = 72

/*Color*/
var YOTSUBA_TEXT = "#800000"
var TOMORROW_BG = "#1D1F21"
var YOTSUBA_BG = "#FFFFEE"
var TOMORROW_TEXT = "#c5c8c6"


/*URLS*/
var URL_BASE = "http://boards.4chan.org/"
// indexes = base+<board>+"/"+<pagenumber> , root = 0
// threads = base+<board>+"/res/"+<resto>

/*http(s)://images.4chan.org/<board>/src/<tim>.<ext>*/
var URL_IMG_BASE = "https://images.4chan.org/"

/* Thumbnails: http(s)://thumbs.4chan.org/<board>/thumb/<tim>s.jpg*/
var URL_THUMB_BASE = "https://thumbs.4chan.org/"
/*Deleted images*/
var URL_IMG_DEL_OP = "https://static.4chan.org/image/filedeleted.gif"
var URL_IMG_DEL_RES = "https://static.4chan.org/image/filedeleted-res.gif"

