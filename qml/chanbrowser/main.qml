﻿import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1
import "constants.js" as UI
PageStackWindow {
    id: appWindow

    showStatusBar: inPortrait
    initialPage: mainPage
    property bool darkMode: false
    property bool nsfwMode: true
    property bool linkifyUrls: false

    //Universal busy indicator
    property bool busy: false

    function invertTheme()
    {
        theme.inverted = !theme.inverted
        darkMode = theme.inverted
    }
    MainPage {
        id: mainPage
    }
    SingleImage{
        id: singleImage
    }
    GalleryPage{
        id: galleryPage
    }

    //For all thread Images from a board or a thread
    ListModel{
        id: imageCatalog
    }
    ToolBarLayout{
        id: mainTools
        ToolIcon{
            platformIconId: "toolbar-back" + (enabled ? "" : "-dimmed")
            onClicked: pageStack.pop()
        }
        ToolIcon{
            platformIconId: "toolbar-favorite-mark"
            onClicked: {

                console.debug("Show favorites")
            }
        }
        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: mainMenu.open()
        }
    }

    Menu{
        id : mainMenu
        visualParent: pageStack
        MenuLayout {
            MenuItem{
                text: "Turn off the light";
                onClicked: { invertTheme(); }
                Switch {
                    id: lightSwitch
                    checked: darkMode
                    anchors.right: parent.right; anchors.rightMargin: UI.MARGIN
                    anchors.verticalCenter: parent.verticalCenter
                    style: SwitchStyle {
                        switchOn: "image://theme/meegotouch-switch-on"+ ((darkMode)? "-inverted":"")
                        switchOff: "image://theme/meegotouch-switch-off"+ ((darkMode)? "-inverted":"")
                    }
                    enabled: false
                    //onCheckedChanged: invertTheme()
                }
            }
            MenuItem{
                Switch {
                    id: linkSwitch
                    checked: linkifyUrls
                    anchors.right: parent.right; anchors.rightMargin: UI.MARGIN
                    anchors.verticalCenter: parent.verticalCenter
                    style: SwitchStyle {
                        switchOn: "image://theme/meegotouch-switch-on"+ ((darkMode)? "-inverted":"")
                        switchOff: "image://theme/meegotouch-switch-off"+ ((darkMode)? "-inverted":"")
                    }
                    enabled: false
                    //onCheckedChanged: { linkifyUrls = linkSwitch.checked }
                }
                text: "Turn plain text to URL"; onClicked: { linkifyUrls = !linkifyUrls; linkSwitch.checked = linkifyUrls }

            }
            MenuItem{
                text: "Show NSFW-boards"; onClicked: { nsfwMode = !nsfwMode; nsfwSwitch.checked = nsfwMode }
                Switch {
                    id: nsfwSwitch
                    checked: nsfwMode
                    anchors.right: parent.right; anchors.rightMargin: UI.MARGIN
                    anchors.verticalCenter: parent.verticalCenter
                    style: SwitchStyle {
                        switchOn: "image://theme/meegotouch-switch-on"+ ((darkMode)? "-inverted":"")
                        switchOff: "image://theme/meegotouch-switch-off"+ ((darkMode)? "-inverted":"")
                    }
                    enabled: false
                    //onCheckedChanged: { nsfwMode = nsfwSwitch.checked;}
                }
            }

        }

    }

    Component.onCompleted: mainPage.getDb()
}
